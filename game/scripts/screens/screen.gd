tool
extends Node

export (PackedScene) var next_scene

onready var tran = $transition/animation

func transition_to():
	var sh = get_tree().change_scene_to(next_scene)
	print(sh)
	pass
